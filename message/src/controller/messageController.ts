/**
 * Created by enixjin on 1/21/2019
 */
import {Controller, encryption, Get, IController, logger, Post, serviceException} from "@jinyexin/core";

@Controller("/message")
export class MessageController {

    constructor() {
    }

    @Get("")
    async list() {
        return {success: true, nodePort: global.config.servicePort};
    }

    @Post({
        url: "",
        callbacks: [async (req, res, next) => {
            logger.debug(req.get("jwt"));
            let user = await encryption.getAuthentication(req);
            if (user.username === "enixjin") {
                next();
            } else {
                res.status(403).jsonp({error: `your are:${user.username}, only enixjin could access this api`});
            }
        }]
    })
    async sendMessage(req) {
        return {success: true, body: req.body, nodePort: global.config.servicePort};
    }

}

export interface MessageController extends IController {
}
