/**
 * Created by enixjin on 4/2/2019.
 */

import * as amqp from "amqplib";
import {Subject} from "rxjs";
import {logger} from "@jinyexin/core";

const rabbitMQServer = amqp.connect("amqp://localhost");

export namespace mqTools {
    export async function sendToQueue(q: string, message: string | Object): Promise<void> {
        return rabbitMQServer
            .then(conn => conn.createChannel())
            .then(ch => {
                return ch.assertQueue(q)
                    .then(ok => {
                        if (typeof message === "object") {
                            logger.warn(`sending message:[${JSON.stringify(message)}] to queue:${q}`);
                            ch.sendToQueue(q, Buffer.from(JSON.stringify(message)));
                        } else {
                            logger.warn(`sending message:[${message}] to queue:${q}`);
                            ch.sendToQueue(q, Buffer.from(message));
                        }
                    });
            })
            .catch(console.warn);
    }

    export function subscribe(q: string): Subject<string> {
        const $message = new Subject<string>();
        rabbitMQServer
            .then(conn => conn.createChannel())
            .then((ch) => {
                return ch.assertQueue(q)
                    .then(ok => {
                        return ch.consume(q, msg => {
                            if (msg) {
                                logger.warn(`receive message:[${msg.content.toString()}] from queue:${q}`);
                                $message.next(msg.content.toString());
                                ch.ack(msg);
                            }
                        });
                    });
            }).catch(console.warn);
        return $message;
    }
}
