/**
 * Created by enixjin on 4/2/2019.
 */
import {logger, Service} from "@jinyexin/core";
import {mqTools} from "../utils/mqTools";

@Service()
export class CustomerService {

    async handleOrderMessage(msg: string) {
        let message = JSON.parse(msg);
        logger.debug(`handle order id:${message.orderid}`);
        let success = false;
        if (success) {
            // success
            await this.updateCustomerFund();
            // current no one interested in this message
            await mqTools.sendToQueue("CustomerFundUpdated", {orderid: message.orderid});
        } else {
            // fail
            await mqTools.sendToQueue("CustomerFundFailed", {orderid: message.orderid});
        }
    }

    async updateCustomerFund() {
    }

}
