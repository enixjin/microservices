/**
 * Created by enixjin on 7/18/16.
 */
global.config = require('../config.js');

import {App, logger} from "@jinyexin/core";

import {CustomerService} from "./customer/customerService";

import {httpTools} from "./utils/httpTools";
import {AuthController} from "./controller/authController";

if (process.argv[2]) {
    global.config.servicePort = parseInt(process.argv[2]);
}

let application = new App(
    {
        appName: "message",
        controllers: [
            AuthController
        ],
        initialDatabase: false,
        enableSwagger: false
    }
);

const endpoint = "/api/auth";

httpTools
    .sendHttp("localhost", 3000, "/api/gateway", "POST", JSON.stringify({
        "name": endpoint,
        "nodes": [
            {"host": "localhost", "port": global.config.servicePort}
        ]
    }))
    .then(() => {
        logger.info(`success connected to gateway`);
        application.init();
        setInterval(() => {
            httpTools.sendHttp("localhost", 3000, "/api/gateway", "PUT", JSON.stringify({
                "name": endpoint,
                "nodes": [
                    {"host": "localhost", port: global.config.servicePort}
                ]
            })).then(logger.silly);
        }, 30 * 1000);
    })
    .catch(() => {
        logger.error("fail to register, exit.");
        process.exit(0);
    });

// subscribe to order create and update Customer balance
import {mqTools} from "./utils/mqTools";

const customerService = new CustomerService();
mqTools.subscribe("OrderCreated").subscribe((msg) => {
    return customerService.handleOrderMessage(msg);
});
