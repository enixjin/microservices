/**
 * Created by enixjin on 1/21/2019
 */
import {Controller, IController, Post, serviceException} from "@jinyexin/core";
import * as jwt from "jsonwebtoken";


@Controller("/auth", false)
export class AuthController {

    constructor() {
    }

    @Post("/login")
    async login(req) {
        if (req.body && req.body.username && req.body.password) {
            return {
                success: true,
                jwt: jwt.sign({username: req.body.username}, global.config.jwtSecKey, {expiresIn: global.config.jwtTimeout}),
                nodePort: global.config.servicePort
            };
        } else {
            throw new serviceException("invalid request!", 400);
        }
    }
}

export interface AuthController extends IController {
}
