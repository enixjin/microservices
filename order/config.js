/**
 * Created by enixjin on 9/24/15.
 */
var config = {};

config.servicePort = 6000;

config.logLevel = 'debug';
config.logFile = '/sandbox/log/microservice/order/log.txt';
config.logLevelFile = "debug";

config.jwtSecKey = 'enixjin';
config.jwtTimeout = '90d';

module.exports = config;
