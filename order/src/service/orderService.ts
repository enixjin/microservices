/**
 * Created by enixjin on 4/2/2019.
 */
import {logger, Service} from "@jinyexin/core";
import {mqTools} from "../utils/mqTools";

@Service()
export class OrderService {

    async handleCustomerFundMessage(msg: string) {
        let message = JSON.parse(msg);
        logger.debug(`handle order fail id:${message.orderid}`);
        await this.compensateOrder(message.orderid);
    }

    async compensateOrder(id: string) {
        // composate order by id
    }

}
