/**
 * Created by enixjin on 1/21/2019
 */
import {Controller, encryption, Get, IController, logger, Post, serviceException} from "@jinyexin/core";
import {mqTools} from "../utils/mqTools";

@Controller("/orders")
export class OrderController {

    constructor() {
    }


    @Post({url: "/"})
    async sendMessage(req) {
        let user = await encryption.getAuthentication(req);
        // 1.create order
        // 2.send to queue
        if (user) {
            await mqTools.sendToQueue("OrderCreated", {orderid: req.body.orderid});
            return {success: true, body: req.body, nodePort: global.config.servicePort};
        }
    }

}

export interface OrderController extends IController {
}
