/**
 * Created by enixjin on 7/18/16.
 */
global.config = require('../config.js');

import {App, logger} from "@jinyexin/core";

import {OrderService} from "./service/orderService";

import {OrderController} from "./controller/orderController";
import {httpTools} from "./utils/httpTools";

if (process.argv[2]) {
    global.config.servicePort = parseInt(process.argv[2]);
}

let application = new App(
    {
        appName: "order",
        controllers: [
            OrderController
        ],
        initialDatabase: false,
        enableSwagger: false
    }
);

const endpoint = "/api/orders";

httpTools
    .sendHttp("localhost", 3000, "/api/gateway", "POST", JSON.stringify({
        "name": endpoint,
        "nodes": [
            {"host": "localhost", "port": global.config.servicePort}
        ]
    }))
    .then(result => {
        logger.info(`success connected to gateway:${JSON.stringify(result)}`);
        application.init();
        setInterval(() => {
            httpTools.sendHttp("localhost", 3000, "/api/gateway", "PUT", JSON.stringify({
                "name": endpoint,
                "nodes": [
                    {"host": "localhost", port: global.config.servicePort}
                ]
            })).then(logger.silly);
        }, 30 * 1000);
    })
    .catch(() => {
        logger.error("fail to register, exit.");
        process.exit(0);
    });

// subscribe to CustomerFundFailed and compensate order
import {mqTools} from "./utils/mqTools";

const customerService = new OrderService();
mqTools.subscribe("CustomerFundFailed").subscribe(msg => {
    return customerService.handleCustomerFundMessage(msg);
});
