module.exports = {
    "apps": [
        {
            "name": "gateway3000",
            "script": "npm",
            "args": "start",
            "cwd": "./gateway",
            "env": {
                "NODE_ENV": "production"
            },
            "instances": 1,
            "exec_mode": "cluster",
            "watch": [
                "./dist"
            ]
        },
        {
            "name": "message4000",
            "script": "npm",
            "args": ["start", "4000"],
            "cwd": "./message",
            "env": {
                "NODE_ENV": "production"
            },
            "instances": 1,
            "exec_mode": "cluster",
            "watch": [
                "./dist"
            ]
        },
        {
            "name": "message4001",
            "script": "npm",
            "args": ["start", "4001"],
            "cwd": "./message",
            "env": {
                "NODE_ENV": "production"
            },
            "instances": 1,
            "exec_mode": "cluster",
            "watch": [
                "./dist"
            ]
        },
        {
            "name": "auth5000",
            "script": "npm",
            "args": "start",
            "cwd": "./auth",
            "env": {
                "NODE_ENV": "production"
            },
            "instances": 1,
            "exec_mode": "cluster",
            "watch": [
                "./dist"
            ]
        },
        {
            "name": "order6000",
            "script": "npm",
            "args": "start",
            "cwd": "./order",
            "env": {
                "NODE_ENV": "production"
            },
            "instances": 1,
            "exec_mode": "cluster",
            "watch": [
                "./dist"
            ]
        }
    ]
};
