/**
 * Created by enixjin on 2/13/2019.
 */
export type Microservice = {
    name: string;
    nodes: Array<{ host: string, port: number, active: number, lastHeartbeat?: Date }>;
}
