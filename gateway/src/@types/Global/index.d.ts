/**
 * Created by enixjin on 1/12/17.
 */
declare module NodeJS {
    interface Global {
        config: any;
        dependencyInjectionContainer: Map<string, any>;
        microservices: Map<string, { name: string, nodes: Array<{ host: string, port: number, active: number, lastHeartbeat?: Date }> }>;
        token: Map<string, string>;
    }
}
