/**
 * Created by enixjin on 1/21/2019
 */
import {Controller, Delete, Get, IController, logger, Post, Put, serviceException} from "@jinyexin/core";
import {Microservice} from "../@types/Microservice";
import {httpTools} from "../utils/httpTools";

const uuidv4 = require('uuid/v4');

@Controller("/gateway", false)
export class GatewayController {

    constructor() {
    }

    @Get("")
    async list() {
        return Array.from(global.microservices.values());
    }

    @Post("")
    async register(req) {
        //TODO check register request format
        let microservice: Microservice = req.body;
        let matchedMicroservice = global.microservices.get(microservice.name);
        if (matchedMicroservice) {
            microservice.nodes.map(newNode => {
                newNode.lastHeartbeat = new Date();
                newNode.active = 1;
                let find = matchedMicroservice.nodes.find(node => node.host === newNode.host && node.port === newNode.port);
                if (!find) {
                    matchedMicroservice.nodes = matchedMicroservice.nodes.concat([newNode]);
                } else {
                    matchedMicroservice.nodes = matchedMicroservice.nodes.map(node => {
                        if (node.host === newNode.host && node.port === newNode.port) {
                            return newNode;
                        } else {
                            return node;
                        }
                    });
                }
            });
            return {success: true, result: "name exists."};
        } else {
            microservice.nodes = microservice.nodes.map(node => {
                node.lastHeartbeat = new Date();
                node.active = 1;
                return node;
            });
            global.microservices.set(microservice.name, microservice);
            return {success: true, result: "name new."};
        }
    }

    @Put("")
    async heartbeat(req) {
        //TODO check register request format
        let microservice: Microservice = req.body;
        logger.silly(JSON.stringify(microservice));
        let matchedMicroservice = global.microservices.get(microservice.name);
        if (matchedMicroservice) {
            microservice.nodes.map(newNode => {
                matchedMicroservice.nodes = matchedMicroservice.nodes.map(node => {
                    if (node.host === newNode.host && node.port === newNode.port) {
                        newNode.active = 1;
                        node.lastHeartbeat = new Date();
                    }
                    return node;
                });
            });
            global.microservices.set(microservice.name, matchedMicroservice);
        }
        return {success: true};
    }

    @Post("/login")
    async login(req) {
        let authService = global.microservices.get("/api/auth");
        if (authService && authService.nodes.filter(node => node.active === 1).length > 0) {
            let nodes = authService.nodes.filter(node => node.active === 1);
            let node = nodes[Math.floor(Math.random() * nodes.length)];
            let resp = await httpTools.proxyHttp(node.host, node.port, "/api/auth/login", "POST", req.body, null);
            let uuid = uuidv4();
            global.token.set(uuid, resp.body.jwt);
            return {token: uuid};
        } else {
            throw new serviceException("no active auth service!", 400, "auth service fail!");
        }
    }

    @Delete("/logout")
    async logout(req) {
        if (req.get("token")) {
            global.token.delete(req.get("token"));
        }
        return {success: true};
    }
}

export interface GatewayController extends IController {
}
