/**
 * Created by enixjin on 2/13/2019.
 */
import {Controller, Get, All, HttpUtils, IController, serviceException, logger} from "@jinyexin/core";
import {httpTools} from "../utils/httpTools";

@Controller("/**", false)
export class ProxyController {

    @All("")
    async proxyAll(clientReq, clientRes) {
        // logger.debug(clientReq.headers.token);
        let name = Array.from(global.microservices.keys()).find(name => clientReq.originalUrl.startsWith(name));
        let microservice = global.microservices.get(name);
        let nodes = microservice ? microservice.nodes.filter(node => node.active === 1) : [];
        let jwtToken = "";
        if (clientReq.get("token")) {
            jwtToken = global.token.get(clientReq.get("token"));
        }
        if (microservice && nodes.length > 0) {
            let node = nodes[Math.floor(Math.random() * nodes.length)];
            let resp = await httpTools.proxyHttp(node.host, node.port, clientReq.originalUrl, clientReq.method, clientReq.body, jwtToken);
            if (resp.statusCode === 500) {
                microservice.nodes = microservice.nodes.map(oldnode => {
                    if (oldnode.host === node.host && oldnode.port === node.port) {
                        oldnode.active = 0;
                    }
                    return oldnode;
                });
                global.microservices.set(microservice.name, microservice);
            }
            clientRes.status(resp.statusCode).jsonp(resp.body);
        } else {
            throw new serviceException(`no live node match url:${clientReq.originalUrl}`, 404);
        }
    }

}

export interface ProxyController extends IController {
}
