/**
 * Created by enixjin on 7/18/16.
 */
import {Microservice} from "./@types/Microservice";

global.config = require('../config.js');

import {App} from "@jinyexin/core";

import {GatewayController} from "./controller/gatewayController";
import {ProxyController} from "./controller/proxyController";

global.microservices = new Map();
global.token = new Map();

let application = new App(
    {
        appName: "gateway",
        controllers: [
            GatewayController,
            ProxyController
        ],
        initialDatabase: false,
        enableSwagger: false
    }
);

setInterval(
    () => {
        global.microservices.forEach((microservice: Microservice) => {
            let now = new Date();
            microservice.nodes = microservice.nodes.map(node => {
                if ((now.getTime() - node.lastHeartbeat.getTime()) >= 60 * 1000) {
                    node.active = 0;
                }
                return node;
            });
        })
    },
    10 * 1000
);

application.init();
